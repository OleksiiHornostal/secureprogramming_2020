## Reverse engineering of dynamic libraries ##
*Objective*: Learn the basics of reverse engineering
### Theory block ###

The result of the development process of modern programs is executable files and dynamic libraries. They contain HEX commands that are unlikely to be understandable to the average person or even an experienced programmer without special training.

In order to analyze executable files and dynamic libraries, you must use special programs:

1.	With a *debugger* you can step through the program assembly interactively.
2.	With a *disassembler*, you can view the program assembly in more detail.
3.	With a *decompiler*, you can turn a program back into partial source code, assuming you know what it was written in (which you can find out with free tools such as [PEiD](http://www.aldeid.com/wiki/PEiD) - if the program is packed, you'll have to unpack it first OR [Detect-it-Easy](http://ntinfo.biz/index.html) if you can't find PEiD anywhere).

For this lab we need to use decompiler to get source code of the program. For different languages there are different decompilers.

1.	Visual Basic: [VB Decompiler](http://www.vb-decompiler.org/), commercial, produces somewhat identifiable bytecode.
2.	Delphi: [DeDe](http://www.softpedia.com/get/Programming/Debuggers-Decompilers-Dissasemblers/DeDe.shtml), free, produces good quality source code.
3.	C: [HexRays](https://www.hex-rays.com/products/decompiler/), commercial, a plugin for IDA Pro by the same company. Produces great results but costs a big buck, and won't be sold to just anyone (or so I hear).
4.	.NET(C#): [dotPeek](http://www.jetbrains.com/decompiler/), free, decompiles .NET 1.0-4.5 assemblies to C#. Support for .dll, .exe, .zip, .vsix, .nupkg, and .winmd files.

There is also another very good software – [Ghidra](https://ghidra-sre.org/) – a free and open source reverse engineering tool developed by the National Security Agency (NSA).

Using these tools, you can check source code and execution logic of any .exe or .dll file. But how we can protect our code against debuggers/disassemblers/decompilers? The main idea of these programs is to give a chance to another developer to analyze your program. 

You can protect you source code and the programs workflow by using specific programs – Obfuscation software. Obfuscation is the act of creating source or machine code that is difficult for humans to understand. In this case, it means changing the logic of the program, changing the names of variables, changing the sequence of function calls, and so on.


### Home Task ###

Using the [external vendor library](https://drive.google.com/open?id=1K9kV9NAwDeSGHvWyBbhYK53NsdQi2MLn) ([alternative link](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/src/lab08/n3k_comm.dll)), complete the following tasks:

1. Determine what language the library is written in (*C*, *C ++*, *C#*, *Delphi*).
2. Define a list of functions and their signatures (for *C* language) that can be used (marked as `Exported Function`).
3. Decompile and normalize / minimize functions: `getIV`,` getIVDB`, `getK`,` getKDB`.
4. Decompile / rewrite function `CRC_16_IBM`. Prove that the results of your function and "their" functions are identical. Describe the algorithm used.
5. Decompile / rewrite the `dec` /` enc` functions. Prove that the results of your functions and "their" functions are identical. Describe the algorithm used.
6. Create a report (.docx file) with code samples, screenshots of the executable file structure and export functions as well as a step-by-step description of all the actions that have been performed.

#### Good luck! ####
