## Editing an executable file ##
### Theory block ###

Most of .exe files in MS Windows have specific PE (Portable Executable) structure. Structure of a PE file on disk is exactly the same as when it is loaded into memory so if you can locate info in the file on disk you will be able to find it when the file is loaded into memory. Each PE file can have specific sections. 
The sections that are most commonly present in an executable (depends on the compiler used or debugger used to analyze the executable) are:

* Executable Code Section, named __*.text*__ (Microsoft) or __*.txt*__ (olydbg) or __*CODE*__ (Borland)
* Data Sections, named __*.data*__, __*.rdata*__, or __*.bss*__ (Microsoft) or __*DATA*__ (Borland)
* Resources Section, named __*.rsrc*__
* Export Data Section, named __*.edata*__
* Import Data Section, named __*.idata*__
* Debug Information Section, named __*.debug*__

There are programs that allow viewing the structure of executable files, making changes to their HEX contents, and even debugging during execution. The best programs are IDA Pro Disassembler and OllyDbg. You can use the to check hex commands in .exe file and change them if you want t change a logic (make a patch for the existing .exe file).


### Home Task ###
* Create a program with a function that displays the string received from the console on the screen. Implement the call of this function directly __*like ‘myFunc (" Hello World ")*__ and through the pointer to the function.
* Determine the size of the call code, taking into account the arguments being pushed onto the stack for both cases.
* Using a hex editor or debugger (for example, Hexedit or Ollydbg), zero out function calls so that their execution is skipped (ex. Replace with NOPs).
* Create a function that enters the password from the keyboard and compares it with the "reference".
* Change the assembly (machine) code of the program in the executable file so that when you enter the wrong password, the program "thinks" that the password is correct and provides access for further work (here you can see the complete guide https://www.youtube.com/ watch? v = _1cEl2IkbR8)
* Create a report (.docx file) with code samples, screenshots of the done steps and a summary of done work.

#### Good luck! ####
