## Study of memory leaks ##
__Objective:__ Investigate the mechanisms of memory leaks and how to deal with them.
### Theory block ###

In computer science, a memory leak is a type of resource leak that occurs when a computer program incorrectly manages memory allocations in such a way that memory which is no longer needed is not released. A memory leak may also happen when an object is stored in memory but cannot be accessed by the running code. A memory leak has symptoms similar to a number of other problems and generally can only be diagnosed by a programmer with access to the programs' source code.

The main idea is to remember always about **free** and **delete** after the memory block is not needed anymore. 

The following example, written in pseudocode, is intended to show how a memory leak can come about, and its effects, without needing any programming knowledge. The program in this case is part of some very simple software designed to control an elevator. This part of the program is run whenever anyone inside the elevator presses the button for a floor.

.
```
When a button is pressed:
  Get some memory, which will be used to remember the floor number
  Put the floor number into the memory
  Are we already on the target floor?
    If so, we have nothing to do: finished
    Otherwise:
      Wait until the lift is idle
      Go to the required floor
      Release the memory we used to remember the floor number
```

The leak in the above example can be corrected by bringing the 'release' operation outside of the conditional:

.
```
When a button is pressed:
  Get some memory, which will be used to remember the floor number
  Put the floor number into the memory
  Are we already on the target floor?
    If not:
      Wait until the lift is idle
      Go to the required floor
  Release the memory we used to remember the floor number

```

There are not many tools and techniques for preventing and timely detection of memory leaks. There is a list of the most common of them:

* **[Smart pointes]( https://docs.microsoft.com/en-us/cpp/cpp/smart-pointers-modern-cpp?view=vs-2019)** – powerfull tool that is used to help ensure that programs are free of memory and resource leaks and are exception-safe:
* **Valgrind** – a programming tool for memory debugging, memory leak detection, and profiling.
* **Deleaker** – memory profiler for C++, C#, .Net and Delphi. Have full integration with Visual Studio and can finds any leaks: memory, GDI, handles and others.
* **Visual Leak Detector (VLD)** – a free, robust, open-source memory leak detection system for Visual C++. It's pretty easy to use. After installing it, you just need to tell Visual C++ where to find the included header and library file.
* **CRT library** – C Run-time Library – the primary tools for detecting memory leaks. It’s easy to use and it very powerfull.

### Home Task ###

1. Write a program that demonstrates memory leaks.
2. Perform comparative analysis of memory leaks using the **valgrind** utility and the `_crtdumpmemoryleaks` function.
3. Add at least one more utility to analyze memory leaks and analyze its usability and efficiency.
5. Rewrite the application snippet to work with **smart** pointers and prove that using them helps reduce memory leaks.
6. Create a report (.docx file) with code samples and a step-by-step description of using tools against memory leaks.

#### Good luck! ####
