## Features of working with printf function ##
__Objective:__ Explore the features of printf function.
### Theory block ###


In C programming language, `printf()` function is used to print the “character, string, float, integer, octal and hexadecimal values” onto the output screen.
We use `printf()` function with %d format specifier to display the value of an integer variable.
Similarly %c is used to display character, %f for float variable, %s for string variable, %lf for double and %x for hexadecimal variable.
To generate a newline, we use “\n” in C `printf()` statement.

There are alsp "% p" and "% x" specifiers in the `printf` family of functions:

* x and X are the output of the hexadecimal number. "x" uses lowercase letters (abcdef), "X" capitalizes (ABCDEF).
* p - pointer output. The output may differ depending on the compiler and the platform.

In 32-bit systems, often one qualifier is used instead of another, which is actually incorrect. Let's look at an example:

.cpp
```cpp
int a = 10;
int *b = &a;
printf("%p\n",b);
printf("%X\n",b);
```
For WIN32 we will have this output (numbers can be different):

`0018FF20`

`18FF20`

For WIN64 we need to use more often %p to avoid splitting:

.cpp
```cpp
size_t Gb = 1024*1024*1024;
char *a = (char *)malloc(2 * Gb * sizeof(char));
char *b = (char *)malloc(2 * Gb * sizeof(char));
printf("use %%X: a=%X\n", a);
printf("use %%X: b=%X\n", b);
printf("use %%p: a=%p\n", a);
printf("use %%p: b=%p\n", b); 
```

Here is the output (numbers can be different):

`use %X: a=80000040`

`use %X: b=40010040`

`use %p: a=0000000080000040`

`use %p: b=0000000140010040`


At the same time, the `printf` function has one very interesting feature: it takes the data for outputting from the stack. However, there are no warnings if the output format (the 1st argument) was specified, but the data itself (another function arguments) was not transmitted, because it would still take something from the stack. Due to this, the attackers have the opportunity to access the data on the stack directly in the program without the use of additional utilities. Let's try to execute the following code:

.cpp
```cpp
#include <iostream>

int main()
{
	int* a = new int(10);
	int* c = new int(5);
	int* i = new int(6);
	int* b = new int(7);
	printf("%08x %08x %08x %08x %08x\n%08x %08x %08x %08x %08x\n%08x %08x %08x %08x %08x\n");

}
```

As we can see, when calling a function, variables were not passed to it for display, however, the format is specified and predefines the output of 3 5-bytes numbers (each with 8-bits) in hexadecimal form. And the information that the program took from the stack is successfully displayed. Thus, it is possible to output data that is hidden in the stack from the eyes of the programmer. A similar problem can be encountered in the case of calling other functions with several parameters, which may be optional.

### Home Task ###
* Create a program using the `printf` function and read the stack data of the current function stack (declared before the function was called).
* Create a function with `printf` usage that cam read the specified amount of data (it should be a function parameter) from a specified memory location that the current program can access.
* **For extra points:** Write down any information of a given memory area that the current program can access.

### Restrictions
The program should take advantage of the features of the `printf` function, and not use standard language features to read or write data in memory at the cursor.
* Create a report (.docx file) with code samples and testing screenshots.

#### Good luck! ####
