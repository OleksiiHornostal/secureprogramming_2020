## Dynamic memory management ##
### Theory block ###

__*PLEASE READ book Secure_Coding_in_C_and_Cpp Chapter 4 paragraph 4.2 and 4.4 before this lab*__

[Book download link](https://drive.google.com/file/d/1_o2EGBNruJXaRsIrAykP1iOU76nRr6X7/view?usp=sharing)

* To manage memory usage in you C/C++ program you can use these functions:

1. malloc;
2. calloc;
3. free;
4. new;
5. delete. 

* There are several common problems that appear when working with memory in modern operating systems:

1. __Initialization errors.__ The modern operating systems don't clear (initialize) memory before allocation so you need to initializa it by yourself (please see book Secure_Coding_in_C_and_Cpp __Example 4.1__).
2. __Failing to check return values.__ If Operating system can't return a memory pointer it can return NULL. You must always check the pointer from alloc function if it is not NULL  (please see book Secure_Coding_in_C_and_Cpp __Example 4.2__).
3. __Dereferencing Null or invalid pointer.__ When we get a pointer we can try to dereference it with cast construction (ex. (int*) ptr). You need to check if pointer is not NULL and if it has the correct type (please see book Secure_Coding_in_C_and_Cpp __Example 4.2__).
4. __Referencing freed memory.__ It is not possible to use memory that was freed (after free or delete) (please see book Secure_Coding_in_C_and_Cpp).
5. __Freeing memory multiple times.__ You can free memoty only once. You cannot double-free it with free or delete (please see book Secure_Coding_in_C_and_Cpp __Example 4.3__).
6. __Memory leaks.__ You need always free memory after its usage. Otherwise, the free memory may run out. But there are some technics to catch memory leaks (please see book Secure_Coding_in_C_and_Cpp __Example 4.4__).
7. __Zero length allocation.__ You can allocate only positive integer bytes of memory and you can not allocate 0 bytes.

### Home Task ###
* Develop a program that demonstrates the problems of using the following functions for working with memory. __At least 5 problems__:

1. malloc;
2. calloc;
3. free;
4. new;
5. delete. 

* Show that (you can use examples from the book):

1. Not all memory allocation functions clear data before usage;
2. Memory release functions do not always destroy data upon release, which may result in the possibility of obtaining information about the value of the destroyed object.

* (advanced) Using virtualization environment (VirtualBox/VMWare/docker etc) demonstrate the features of the following functions on at least 2 OS for at least two compilers (eg. Microsoft Visual C, gcc4, gcc5+, Borland C etc).

* __Create a report__ (.docx file) with screenshots of the results and write a summary of the various compilers used to allocate and free memory.

#### Good luck! ####