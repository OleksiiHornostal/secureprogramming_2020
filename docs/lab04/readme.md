## Features of working with the Constructors and Destructors in C++ ##

__Objective:__ To gain knowledge of the features of C ++ Constructors and Destructors

### Theory block ###

Constructors are special class functions which performs initialization of every object. The Compiler calls the Constructor whenever an object is created. Constructors initialize values to object members after storage is allocated to the object.

Whereas, Destructor on the other hand is used to destroy the class object.

Thus, in Constructors, dynamic memory is allocated for fields inside the class, and in Destructors, all previously requested memory must be returned back to the system (released).

In this case, you should understand the features of the work of Constructors and Destructors for derivative classes. Constructors and Destructors are called in a chain, and therefore an exception that occurred during the execution of the child class destructor can lead to undefined consequences in the program, because the base class destructors have not yet been called.

The same problems arise with the invocation of ordinary functions, in which an exception may be thrown before the memory is freed.

In this regard, it is necessary to get acquainted with such a term as ‘stack unwinding’. As you create objects statically (on the stack as opposed to allocating them in the heap memory) and perform function calls, they are "stacked up".

When a scope (anything delimited by __{__ and __}__) is exited (by using __return XXX;__, reaching the end of the scope or throwing an exception) everything within that scope is destroyed (destructors are called for everything). This process of destroying local objects and calling destructors is called stack unwinding.

### Home Task ###

* Develop a program that works with Constructors and Destructors. Develop a class hierarchy: A is the base class, B and C are its inheritors. In each class, it is necessary to create constructors in which the local field of the class (for example, an array) is initialized and memory is allocated for it. In destructors, it is necessary to free the received memory.
* Implement the behavior in the constructor and destructors, which causes an exceptional situation (ex. throwing any exception).
* Research the process of the ‘stack unwinding’.
* In the developed program, examine and bring the research results (algorithm), how the process of implicitly calling constructors and destructors is performed (creating and destroying objects at the stack level, calling the `delete` method, etc.).

#### Good luck! ####
