## Protection against binary file changes ##
__Objective:__ Learn how to sign executable files
### Theory block ###

As you can already know from previous tasks in this course, there are a huge number of real software vulnerabilities that cybercriminals can exploit to harm your program. Most often, attackers change the execution of commands in the executable file to force it to do what they need, for example, to bypass password protection or to obtain personal or secret data.

One of the simple, but quite effective ways to protect executable files is to use a signature. Digital signatures are an algorithm for authenticating files in computer systems using cryptographic systems with a public key (asymmetric encryption).

There are a large number of ready-made software solutions for using digital signatures in modern operating systems. For Windows OS, there is a simple and convenient utility called Signature Tool.

[Some instructions where you can find SignTool in Windows](https://stackoverflow.com/questions/31869552/how-to-install-signtool-exe-for-windows-10)

### Home Task ###
* Create a demo program using the C / C ++ programming language (eg, calculator).
* Organize the signature of an executable file written in *C* or *C++* and code-level verification using the *SignTool* utility.
* Make sure that changing the signed file makes it impossible to execute.
* Create a report (.docx file) with code samples, screenshots of the executable file signing process and checking whether the file can be executed after it has been changed.

#### Good luck! ####
