## Working with strings. Stack corruption. Buffer owerflow attack (injection) ##
### Theory block ###

__*PLEASE READ book Secure_Coding_in_C_and_Cpp Chapter 2 paragraph 2.2 and 2.3 before this lab*__

[Book download link](https://drive.google.com/file/d/1_o2EGBNruJXaRsIrAykP1iOU76nRr6X7/view?usp=sharing)

* Any string is a sequence of chars that ends with special ending symbol \0. In modern operating systems strings are stored in memory as char arrays even if we have specific class for them (std::string in c++, string in c#, String in Java). You need to understand that all these classes incapsulate char arrays. Therefore, when we work with strings, we face the same problems as when we work with arrays
* There are some __common functions which we use to process strings__ in our c/c++ programs:

1. To get strings as user input: *gets(), scanf()*
2. To copy and concatenate: *strcpy(), strcat(), sprintf()*
3. To output some strings: *puts(), printf()*

* There are some main problems which you can face when you are working with string:

1. __Wrong string length limit:__ when you create a limited buffer but gets strings that is longer than that buffer.
2. __Off-by-one errors:__ when you forget about actual string length and about the fact that string length doesn't include \0 terminator.
3. __Null-terminator errors:__ if you create a string as char array you can't forget about \0 symbol at the end because all common functions search this symbol and if it is not resented they can fail. 
4. __String Truncation issue:__ when you copy one string to anither the second string should be big enough to store all characters from the first one.

* Anyone can use the above problems to attack the computer and its operating system. The most famous exploits are:

1. __Corrupted data:__ if you know that you can enter more text than the program expects and thereby overwrite system information on the stack (ex. replace function return address), then you can intentionally harm (atack) the program or the entire computer (check paragraph 2.3 Example 2.3 and Figure 2.4)
2. __Buffer Overflows:__ when you try to access memory outside allocated block
3. __Invalidate memory structures:__ every process (running program) has specific memory strucutere in operating system. In this case, anybody can try to put invalid data to the stack structure to corrupt stack and change program default workflow (ex. the correct password could be replaced in memory with some other value).
4. __Code injections:__ there is a possibility to change stack info using strings. The program commands are also stored in stack so they can be replaced by anybody and program will work another way. 

* There are some common strategies that allow to prevent issues when working with strings in C/C++. You can check them in paragraph 2.4.

### Home Task ###
* Using the VC compiler from MS Visual Studio to create a program that demonstrates problem, where overflow of the array variable at the stack level in the function can lead to the destruction of the stack – when trying to access an invalid address in memory and using strings. Please check Paragraph 2.2 and example 2.1 from the book as an example.
* Prove that this problem is related to the data that is written to the array using the Memory Window in Visual Studio (please check this link https://docs.microsoft.com/en-us/visualstudio/debugger/memory-windows?view=vs-2019). Make a screenshots of stack memory before and after stack overflow. 
* Use the BufferSecurityCheck security system in the compiler arguments (`/ GS` for MSVS and` -fno-stack-protector` for GCC) to check how you can avoid possible overflow problems. Please note that in recent versions of Visual Studio this system can be used by default. In this case, to do steps 1 and 2, use the /GS- option, and for paragraph 3, use / GS option
* Create a report (.docx file) with code samples, screenshots of the results and a summary of the researched problems and ways how they can be resolved.

#### Good luck! ####
