## Data hiding technology Rar-Jpeg ##

### Objective: To explore the possibility of "hiding" archives in images

### Theory block ###


* __RARJPEG__ is a special kind of file container: a JPEG illustration, to which the RAR archive is added at the end (to the same file). Depending on the extension, such a file can be perceived by various programs both as a JPEG illustration and as a RAR archive. This circumstance allows, for example, to use image boards (accepting only illustrations) as anonymous file hosting for archives.
* The same method can be used for some other formats: JPEG, GIF, PNG or WAV sound recordings can be supplemented with RAR or ZIP archives or MP3 sound recordings. However, not every archiver is suitable for opening RAR archives and especially ZIP archives from such a file - WinRAR, however, can do it.
* Technologically, this method is based on the fact that the program that reads the first file ignores all data following its logical end (and therefore ignores the entire second file), and the program that reads the second file aggressively searches for its header and ignores everything that precedes the header (and thereby able to first skip the entire first file, not at all embarrassed, and then start reading immediately from the second).
* On DOS and Windows systems, such files can be created using the merge binary copy command:

*copy / b file1.jpg + file2.rar result.jpg*

* This command, however, does not produce a visible error if the second file is missing, but simply creates a result that is identical to the first file - so inevitably you have to be vigilant.

* In * nix-like systems, this operation is done as follows:

*cat file1.jpg file2.rar> result.jpg*

![Byte Structure Of RarJpeg Files](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/raw/ae1df4df06c5bc07b86caf537fc5a431f3ea69ad/docs/lab10/media/ByteStructureOfRarJpegFiles.png)

### Home Task ###
* Develop a program that can create and decode back (restore RAR archives) RarJpeg files without any third party libraries (only working with files). Demonstrate its work in report.
* Develop a program (or program block for the first program) that can detect RarJpeg files by Jpeg and Rar hex signatures (please check this link to get signatures list https://en.wikipedia.org/wiki/List_of_file_signatures and https://en.wikipedia.org/wiki/JPEG#Syntax_and_structure). Also you can use the image above to understand how to determine that an archive is hidden in a particular image by analyzing its contents by bytes 
* Give examples of other similar data hiding technologies.
* Create a report (.docx file) with code samples, screenshots of the results and examples of other similar technologies.

#### Good luck! ####
