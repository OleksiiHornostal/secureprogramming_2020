## Using buffer overflow vulnerability in C language ##
### Theory block ###

__*PLEASE READ book Secure_Coding_in_C_and_Cpp Chapter 2 paragraph 2.3 before this lab*__

[Book download link](https://drive.google.com/file/d/1_o2EGBNruJXaRsIrAykP1iOU76nRr6X7/view?usp=sharing)

You already know that there are some main problems which you can face when you are working with strings. The most common problem is related to the wrong string length limit what may cause buffer overflow issue. 

If the string that is exactly the array of characters is being copied to the memory and the memory block is not the long enough it may replace the system info in stack and some useful commands. 

This is how the Buffer Overflow Attack can be performed. It does it possible, for example, to overwrite the password verification command in the program or change its executing workflow.

### Home Task ###

* Create a program with a sample 2.3 from the book from the paragraph 2.3.

.C     
```cpp
bool isPasswordOk() {
	char password[12];
	gets(password);
	return 0 == strcmp(password, "goodpass");
}

int main(int argc, char* argv[]) {
	bool status;
	puts("Enter password: ");
	status = isPasswordOk();
	if (!status) {
		puts("Access denied");
		exit(-1);
	}
	puts("Passed");
	return 0;
}
```


* Using the debugger and not entering the correct password, but using the buffer overflow error, destroy / rewrite the call stack so that the program returns `Passed` if the character sequence is incorrectly entered. You will need to examine the executable file with IDA Debugger or OllyDbg software [Check additional example here]( https://www.thegeekstuff.com/2013/06/buffer-overflow/).
* Create a report (.docx file) with code samples, screenshots of the results and a summary of the researched attack and how it can be prevented in future.

#### Good luck! ####
