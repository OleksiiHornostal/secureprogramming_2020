# Secure Programming 2020 #

This README would explain you everthing about this coursre and our working plans.

### We will have 16 study weeks and 10 themes (some of them will take several weeks). I will add links over time ###

1. Week  1.		[Dynamic memory management](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab01/readme.md)
2. Week  2.		[Working with strings. Stack corruption. Buffer owerflow attack (injection)](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab02/readme.md)
3. Week  3.		[Editing an executable file](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab03/readme.md)
4. Weeks 4-5.	[Features of working with the Constructors and Destructors in C++](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab04/readme.md)
5. Week  6.		[Features of working with printf function](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab05/readme.md)
6. Week  7.		[Study of memory leaks](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab06/readme.md)
7. Week  8.		[Protection against binary file changes](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab07/readme.md)
8. Weeks 9-12.	[Reverse engineering of dynamic libraries](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab08/readme.md)
9. Weeks 13-15.	[Using buffer overflow vulnerability in C language](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab09/readme.md)
10. Week 16.	[Data hiding technology Rar-Jpeg](https://bitbucket.org/OleksiiHornostal/secureprogramming_2020/src/master/docs/lab10/readme.md)

Every week I will provide you theory and tasks via links above. You need to read theory carefully, do your homework, upload it to the bitbucket repository and create a report whis also should be uploaded to the bitbucket.

### What do you need to do to start? ###
* Create a bitbucket account and create a repository 
* Add me in Repository settings (users and permissions tab) to the repository as Admin - gornostalaa@gmail.com
* Create 2 folders in your repository: src and docs. You will upload you labs in src folder and you reports in docs folder. 
* Download and install MS Visual Studio Community Edition 2019 and add C++ package during installation process.
* [Fill a row in Google Spreadsheet (First/Last Names and Bitbucket url)](https://docs.google.com/spreadsheets/d/1qGfFEG0ai_dLAKPn9-edfHjaljQoowfm105RGUtRttc/edit?usp=sharing  "Spreadsheet link")
* [Download this book - I will use it in my theory block](https://drive.google.com/file/d/1_o2EGBNruJXaRsIrAykP1iOU76nRr6X7/view?usp=sharing)

### Folders and files naming rules ###
* All C++ projects for your labs should go to the src folder as SecureProgramming_LastName_01
* All reports should be formatted as docx (Win Office) document as SecureProgramming_LastName_01.docs
* [Lab Report example](https://drive.google.com/file/d/1CmG1u4n31LJ_3ON0mDTBPIFdLYIloIW1/view?usp=sharing)

###### Good luck! ######